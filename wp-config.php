<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pathways_pathwa');

/** MySQL database username */
define('DB_USER', 'pathways_pathwa');

/** MySQL database password */
define('DB_PASSWORD', '@pathways1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Jd!@KKC@};&/1^:l=Ho!?Y#P3S0cLiB}]s*:.GL+VrKn]q$ISLHs;|fs|sf.YAmn');
define('SECURE_AUTH_KEY',  'N4Tg&z-5N_NY{qZd/UVXU%Yb2vOn;.^I>2T^^GebK@hz)nZutzYZcUQ2HPo3k;qN');
define('LOGGED_IN_KEY',    'dbOAhAB^}{seNkOiUQ<;. UdW~Jy]+f8mH0IS9t,wZ| &D4dEt8$@ld+a)}$#uMp');
define('NONCE_KEY',        '>)[]A|[,2^y{ZNZ_G5Yr-jcwW{vSt_>M{b.(uxWON{25wL?TY7?H2,9lQI8n7RZ!');
define('AUTH_SALT',        '!Dy$UpD3FOndyC~_xy4@? )~}ZZ&gi(Y(=% -%0k:T*&MlH:nv)85mX)%xz%m[I{');
define('SECURE_AUTH_SALT', 'gT&EN.tFM2=EE]:L$@;EyYcMXFIU#b}F!Yx3OYQ<Y;am2QZ8OnxV[V~x5zsr(eB]');
define('LOGGED_IN_SALT',   'VXcuX`KacwSS]_U,1hsJD_5[iHht.Df7.,*H}+z)MFd(!{<P#v+![]?l_>mSXD;o');
define('NONCE_SALT',       'QsoE)H;3=7A~eX3}9<,]gr|/SCkS}a4U~gPP}X(dZ%`2<rK/MQ;S]A$-0.p6Go?4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
